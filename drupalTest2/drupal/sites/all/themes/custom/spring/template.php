<?php

        function spring_preprocess_page(&$variables) {
            /*  
            if sidebar left is occupied add class of one-fourth to sidebar_left
            add class three-quarters to content
            else add class full-width to content
            add class "" to sidebar_left
            
            
            
            Next week do a right sidebar
            
            Check if both sidebars are occupied,
            if so , set them to 20%
            set content to 50%
            
            check if right sidebar is populated and left is not
            if so, set content to 75%
            right sidebar 25%
            
            if left sidebar_left is occupied and sidebar_right is occupied {
            add a class of one-fourth to sidebar-left
            add class of one-fourth to sidebar-right
            add class one-half to content
            } else if sidebar_left is occupied and sidebar_right is not{
                add class of one-fourth to sidebar_left
                add class of three-fourths to content
                add class of '' to sidebar_right
            
            } else if sidebar_left is not occupied and sidebar_right is {
                add class of one-fourth to sidebar_right
                add class of three-fourths to content
                add class of '' to sidebar_left 
            }

            else {
                add class full-width to content
                add class of '' to sidebar_left
                add class of '' to sidebar_right
            }
            
            
            
            */
        
            
            
                $page = $variables['page'];
                $sidebar_class_left = "one-fourth";
                $sidebar_class_right = "one-fourth";
                $content_class = "one-half";
                $content_class = "three-fourths";
                $variables['page']['tpl_control'] = [];
            
                if (!empty($page['sidebar_left']) && !empty($page['sidebar_right'])) {
                        $sidebar_class_left = "one-fourth";
                        $sidebar_class_right = "one-fourth";
                        $content_class = "one-half";
                } else if(empty($page['sidebar_left']) && !empty($page['sidebar_right'])) {
                        $sidebar_class_left = "";
                        $sidebar_class_right = "one-fourth";
                        $content_class = "three-fourths";
                } else if(!empty($page['sidebar_left']) && empty($page['sidebar_right'])) {
                        $sidebar_class_left = "one-fourth";
                        $sidebar_class_right = "";
                        $content_class = "three-fourths";
                } else {
                        $sidebar_class_left = "";
                        $sidebar_class_right = "";
                        $content_class = "full-width";
                }
            
            
                $variables['page']['tpl_control']['sidebar_class_left'] = $sidebar_class_left;
                $variables['page']['tpl_control']['sidebar_class_right'] = $sidebar_class_right;
                $variables['page']['tpl_control']['content_class'] = $content_class;
            
        }

?>